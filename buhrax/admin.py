from django.contrib import admin

# Register your models here.

from .models import Types_Of_Expenses, Expense, Cash, Cash_Planned


class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('expenditure_name', 'expense_type', 'description')


class CashAdmin(admin.ModelAdmin):
    list_display = ('cash_expense', 'cash_date', 'cash_type', 'cash_value')


class Cash_Planned_Admin(admin.ModelAdmin):
    list_display = ('planned_cash_expense', 'planned_cash_date', 'planned_cash_repeat', 'planned_cash_value')

admin.site.register(Types_Of_Expenses)
admin.site.register(Expense, ExpenseAdmin)
admin.site.register(Cash, CashAdmin)
admin.site.register(Cash_Planned, Cash_Planned_Admin)

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from datetime import date
from django.conf import settings
from django.contrib.auth.models import User


# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __unicode__(self):
        return unicode(self.user.username)


class Types_Of_Expenses(models.Model):
    expense_name = models.CharField(max_length=200)
    expense_color = models.CharField(max_length=10)

    class Meta:
        db_table = 'buhrax_types_of_expenses'
        verbose_name = 'Типы расходов'
        verbose_name_plural = 'Типы расходов'

    def __unicode__(self):
        return unicode(self.expense_name)


class Expense(models.Model):
    expense_type = models.ForeignKey(Types_Of_Expenses)
    expenditure_name = models.CharField(max_length=200)
    description = models.CharField(
        max_length=250, default='описание статьи затрат')

    class Meta:
        db_table = 'buhrax_expense'
        verbose_name = 'Статьи расходов'
        verbose_name_plural = 'Статьи расходов'

    def __unicode__(self):
        return unicode(self.expenditure_name)


class Cash(models.Model):
    CASH_TYPE_CHOICES = (
        ('P', 'Приход'),
        ('R', 'Расход'),
    )
    cash_expense = models.ForeignKey(Expense, default=None)
    cash_date = models.DateField(default=date.today)
    cash_type = models.CharField(max_length=1, choices=CASH_TYPE_CHOICES)
    cash_value = models.FloatField(default=0.0)
    cash_desc = models.CharField(
        max_length=200, default='комментарий к движению средств')

    class Meta:
        db_table = 'buhrax_cash'
        verbose_name = 'Деньги'
        verbose_name_plural = 'Деньги'

    def __unicode__(self):
        return unicode(self.cash_expense)


class Cash_Planned(models.Model):
    PLANNED_CASH_TYPE_CHOICES = (
        ('N','Разово'),
        ('W', 'Неделя'),
        ('M', 'Месяц'),
        ('Y', 'Год')
    )
    planned_cash_expense = models.ForeignKey(Expense, default=None)
    planned_cash_date = models.DateField(default=date.today)
    planned_cash_repeat = models.CharField(max_length=1, choices=PLANNED_CASH_TYPE_CHOICES)
    planned_cash_value = models.FloatField(default=0.0)
    planned_cash_desc = models.CharField(
        max_length=200, default='комментарий к движению средств')

    class Meta:
        db_table = 'buhrax_planned_cash'
        verbose_name = 'Запланированные затраты'
        verbose_name_plural = 'Запланированные затраты'

    def __unicode__(self):
        return unicode(self.planned_cash_expense)
